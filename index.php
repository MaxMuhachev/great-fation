<?php

require 'application/lib/Dev.php';
require 'application/core/Exception.php';
require 'application/core/PHPMailer.php';
require 'application/core/SMTP.php';

use application\core\Router;

spl_autoload_register(function($class) {
    $path = str_replace('\\', '/', $class.'.php');
    if (file_exists($path)) {
        require $path;
    }
});

session_start();

$router = new Router;
$router->run();
