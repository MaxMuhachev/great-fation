-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 21 2019 г., 12:34
-- Версия сервера: 10.3.11-MariaDB
-- Версия PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `great_fation`
--

-- --------------------------------------------------------

--
-- Структура таблицы `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `id_img` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `src` varchar(30) NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_img`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `images`
--

INSERT INTO `images` (`id_img`, `src`, `id_product`) VALUES
(1, '1.jpg', 1),
(22, '7_1.jpg', 5),
(3, '1_3.jpg', 1),
(4, '2.jpg', 2),
(5, '2_2.jpg', 2),
(6, '2_2.jpg', 2),
(25, '8.jpg', 6),
(8, '5.jpg', 4),
(9, '5_1.jpg', 4),
(10, '5_2.jpg', 4),
(27, '10.jpg', 7),
(26, '9.jpg', 6),
(23, '7.jpg', 5),
(21, '6_1.jpg', 3),
(20, '6.jpg', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id_order` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(10) UNSIGNED NOT NULL,
  `price` int(11) NOT NULL,
  `count` tinyint(11) NOT NULL DEFAULT 1,
  `name` varchar(50) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id_order`, `product_id`, `price`, `count`, `name`, `phone`, `email`) VALUES
(25, 2, 8990, 1, 'Максим', '89824481880', 'macsim-1996@yandex.ru'),
(26, 22, 1819, 1, 'Максим', '89824481880', 'macsim-1996@yandex.ru'),
(27, 2, 8990, 1, 'Максим', '89824481880', 'macsim-1996@yandex.ru'),
(28, 22, 1819, 1, 'Максим', '89824481880', 'macsim-1996@yandex.ru'),
(29, 2, 8990, 1, 'Максим', '89824481880', 'macsim-1996@yandex.ru'),
(30, 22, 1819, 1, 'Максим', '89824481880', 'macsim-1996@yandex.ru'),
(31, 2, 8990, 1, 'Максим', '89824481880', 'macsim-1996@yandex.ru'),
(32, 22, 1819, 1, 'Максим', '89824481880', 'macsim-1996@yandex.ru'),
(33, 4, 400, 1, 'name', '34542', 'nolijoleta@next-mail.info'),
(34, 2, 8990, 1, 'name', '34542', 'nolijoleta@next-mail.info'),
(35, 4, 400, 1, 'name', '34542', 'macsim-1996@yandex.ru'),
(36, 2, 8990, 1, 'name', '34542', 'macsim-1996@yandex.ru'),
(37, 4, 400, 1, 'name', '34542', 'macsim-1996@yandex.ru'),
(38, 2, 8990, 1, 'name', '34542', 'macsim-1996@yandex.ru'),
(39, 4, 400, 1, 'name', '34542', 'macsim-1996@yandex.ru'),
(40, 2, 8990, 1, 'name', '34542', 'macsim-1996@yandex.ru'),
(41, 4, 400, 1, 'name', '34542', 'macsim-1996@yandex.ru'),
(42, 2, 8990, 1, 'name', '34542', 'macsim-1996@yandex.ru'),
(43, 2, 8990, 1, 'Максим', '112', 'macsim-1996@yandex.ru');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id_product` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_product` varchar(50) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `price` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `similar` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id_product`, `name_product`, `description`, `price`, `active`, `similar`) VALUES
(1, 'Платье с открытыми плечами', 'Платье с открытыми плечами', 9990, 1, 1),
(2, 'Пальто Burton Menswear London', 'Пальто выполнено из полушерстяного текстиля. Модель приталенного силуэта. Детали: лаконичный дизайн, однобортная застежка на пуговицах, 2 наружных кармана и 1 карман внутренний, спинка со шлицей, шелковистая подкладка.\r\nСостав: Шерсть - 51%, Полиэстер - 44%, Нейлон - 4%, Акрил - 1%\r\nСезон: демисезон\r\nЦвет: синий\r\nУзор: однотонный\r\nСтрана производства: Вьетнам\r\nЗастежка: пуговицы\r\nАртикул BU014EMDBDI7', 8990, 1, 0),
(3, 'Платье &quot;Лето&quot;', 'diskr', 400, 1, 0),
(4, 'Платье Арт.', 'Состав 50% хлопок, 30% полиамид, 20% вискоза, Отделка: 100% полиэстер, Подкладка: 100% полиэстер\r\nСтрана-производитель Китай', 2990, 1, 0),
(5, 'Платье Арт. 92240055058', 'Состав 50% хлопок, 30% полиамид, 20% вискоза, Отделка: 100% полиэстер, Подкладка: 100% полиэстер\r\nСтрана-производитель Китай', 2990, 1, 0),
(6, 'Платье Арт. 9122603508', 'Состав 72% вискоза, 28% полиэстер\r\nСтрана-производитель Китай', 1819, 1, 0),
(7, 'Платье-мини из смешанного шелка', 'Платье-мини с длинными рукавами, вырезом на груди и поясом-шнуром на талии сшито из струящегося смешанного шелка белого цвета с красочным цветочным узором.\r\nЭтот изысканный и нежный вариант в романтическом стиле дополнит небольшая колоритная сумка и обувь на каблуке.\r\n\r\nСостав: 83% вискоза и 17% шелк\r\nУход: профессиональная чистка', 20990, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id_role` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_role` varchar(20) NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `role`
--

INSERT INTO `role` (`id_role`, `name_role`) VALUES
(1, 'admin');

-- --------------------------------------------------------

--
-- Структура таблицы `similar_t`
--

DROP TABLE IF EXISTS `similar_t`;
CREATE TABLE IF NOT EXISTS `similar_t` (
  `id_similar` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_similar_product` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_similar`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `similar_t`
--

INSERT INTO `similar_t` (`id_similar`, `id_product`, `id_similar_product`) VALUES
(43, 1, 4),
(42, 1, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id_users` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id_users`, `login`, `email`, `password`) VALUES
(1, 'max12_23', 'macim-1996@yandex.ru', '202cb962ac59075b964b07152d234b70'),
(3, 'Дима', NULL, 'e10adc3949ba59abbe56e057f20f883e');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
