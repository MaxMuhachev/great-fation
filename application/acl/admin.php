<?php

	/*
	 * Файл с доступом к страницам и категории
	*/

	return [
		'all' => [

		],
		'admin' => [
			'admin',
			'list',
			'product',
			'add',
			'order',
			'change',
			'admins',
			'addadmin'
		],
	];
