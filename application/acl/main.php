<?php

return [
	'all' => [
		'index',
		'product',
		'cart',
	],
	'admin' => [
		'index',
		'product',
		'cart',
	],
];
