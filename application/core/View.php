<?php

namespace application\core;

class View {

	public $path;
	public $route;
	public $layout = '';

	public function __construct($route) {
		$this->route = $route;
		$this->path = $route['controller'].'/'.$route['action'];
	}

	/*Вызов view*/
	public function render($title, $vars = []) {
		extract($vars);
		$path = 'application/views/'.$this->path.'View.php';
		if (file_exists($path)) {
			ob_start();
			require $path;
			$content = ob_get_clean();
			require 'application/views/layouts/default'.$this->layout.'View.php';
		}
	}

	/*Вызов view с изменением количества товаров*/
	public function renderMain($title, $cart, $vars = []) {
		extract($vars);
		$path = 'application/views/'.$this->path.'View.php';
		if (file_exists($path)) {
			ob_start();
			require $path;
			$content = ob_get_clean();
			require 'application/views/layouts/default'.$this->layout.'View.php';
		}
	}

    /*Перенаправление*/
    public function redirect($url) {
		header('location: '.$url);
		exit;
	}

	/*Вывод ошибки*/
	public static function errorCode($code) {
		http_response_code($code);
		$path = 'application/views/errors/'.$code.'.php';
		if (file_exists($path)) {
			require $path;
		}
		exit;
	}

}
