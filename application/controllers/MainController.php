<?php

namespace application\controllers;

use application\core\Controller;

class MainController extends Controller {

	public function indexAction() {
		$cart = $this->getCartProduct();
		if (isset($_GET['page'])){
			$page = $_GET['page'];
		}
		else{
			$page = 0;
		}
		$this->view->renderMain('Главная страница', $cart,  $this->model->getProducts($page));
	}

	public function productAction() {
		$this->view->layout = "Product";
		$prod = [
			'product' => $this->model->getMyProduct($_GET['id']),
			'similar' => $this->model->similarProduct($_GET['id']),
		];

		$cart = $this->getCartProduct();
		$this->view->renderMain('Карточка товара', $cart, $prod);
	}

	public function cartAction() {

		//Удаление товара из корзины
		if (isset($_POST['clean']) && isset($_SESSION['cart'])){
			for ($i = 0; $i < count($_SESSION['cart']); $i++){
				if ($_SESSION['cart'][$i] == $_POST['clean']){
					unset($_SESSION['cart'][$i]);
				}
			}
		}

		// Получение количества товара в корзине
		$products = [0];
		if (isset($_SESSION['cart'])){
			$products = $this->model->getCartProduct($_SESSION['cart']);
		}

		$cart = $this->getCartProduct();

		if (!isset($_POST['email'])){
			$this->view->renderMain('Корзина', $cart, $products);
		}
		else{
			// Отправка письма
			if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['phone']) && isset($_SESSION['cart'])){
				$products = $this->model->getCartProduct($_SESSION['cart']);
				$prodAll = '';
				$priceAll = 0;
				$orderIdAll = "";
				foreach ($products['products'] as $prod){
					$prodAll .= $prod['name'].": ". $prod['price'] ."руб., ";
					$orderIdAll = $orderIdAll. $this->model->addOrder($prod['id'], $prod['price'], $prod['count'], $_POST['name'], $_POST['phone'], $_POST['email']).", ";
					$priceAll += $prod['price'];
				}
				$this->model->send_post($_POST['email'], $_POST['name'], $prodAll, $priceAll, $orderIdAll); //Отправка письма на почту заказчику

				unset($_SESSION['cart']); //Очищение списка товаров в корзине
				session_destroy();
				session_start();
				$this->view->renderMain('Корзина<p class="itog" style="font-size: 25pt">Письмо отправлено вам на почту! Наш менеджер с вами свяжимся.<p>', $cart, [0]);
			}
			else{
				if (isset($_POST['email'])){
					$this->view->renderMain('Корзина<p class="itog" style="font-size: 25pt">Заполните все поля!<p>', $cart, $products);
				}
			}
		}
	}

	// Получение количества товаров в корзине
	private function getCartProduct(){
		if (isset($_POST['cart_id'])){
			if (!isset($_SESSION['cart']))
			{
				$_SESSION['cart'] = [$_POST['cart_id']];
			}
			else{
				array_push($_SESSION['cart'], $_POST['cart_id']);
			}
		}
		$cart = 0;
		if (isset($_SESSION['cart'])){
			foreach ($_SESSION['cart'] as $id){
				$cart++;
			}
		}
		return $cart;
	}
}
