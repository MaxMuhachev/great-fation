<?php
/*
 * Контроллер для панели администратора
 */
namespace application\controllers;

use application\core\Controller;


class AdminController extends Controller{

	// Получение всех товаров в админ панели
    public function listAction(){
        if (isset($_GET['delprod'])){
			// Удалить товар
			$this->view->layout = "Admin";
			$this->model->delProduct($_GET['delprod']);
			$this->view->render("Товар", $this->model->getProducts());
		}
        else{
			// Получение всех товаров в админ панели
			$this->view->layout = "Admin";
			$this->view->render("Товары", $this->model->getProducts());

		}
    }

	// Получение выбранного товара в админ панели
	public function productAction()
	{
		if (isset($_POST['delSimilar'])){
			$this->model->delSimProducts($_POST['delSimilar'], $_POST['prod_id']);
		}
		else{
			if (isset($_POST['addSimilar'])){
				$product = json_encode($this->model->addSimilarProducts(addslashes($_POST['addSimilar']),$_POST['prod_id']));
				echo $product;
			}
			else{
				// Если отправили форму
				if (isset($_POST['name'])) {
					$this->model->updateProduct(htmlspecialchars($_POST['name']), htmlspecialchars($_POST['description']), $_FILES['photo'],
						$_POST['price'], $_POST['active'], $_POST['product_id']);
					$this->view->redirect('list');
				}
				else{
					if (isset($_POST['src'])){
						$this->model->delImage($_POST['prod_id'], $_POST['src']);
					}
					else{
						$this->view->layout = "Admin";
						$prod = [
							'product' => $this->model->getMyProduct($_GET['id']),
							'similar' => $this->model->similarProduct($_GET['id']),
							'all' =>$this->model->getSimilarProducts($_GET['id'])
						];
						$this->view->render("Изменение товара", $prod);
					}
				}
			}
		}
	}

	// Добавление товара
	public function addAction()
	{
		if (isset($_POST['delSimilar'])){
			$this->model->delSimProducts($_POST['delSimilar'], $_POST['prod_id']);
		}
		else{
			if (isset($_POST['addSimilar'])){
				$product = json_encode($this->model->addSimilarProducts($_POST['addSimilar'],$_POST['prod_id']));
				echo $product;
			}
			else{
				// Если отправили форму
				if (isset($_POST['name'])){
					$this->model->addProduct(htmlspecialchars($_POST['name']), htmlspecialchars($_POST['description']), $_FILES['photo'],
						$_POST['price'], $_POST['active']);
					$this->view->redirect('list');
				}
				else{
					$this->view->layout = "Admin";
					$this->view->render("Добавлние товара", $this->model->getAllProducts());
				}
			}
		}
	}

	// Получение всех заказов
	public function orderAction()
	{
		$this->view->layout = "Admin";
		$this->view->render("Заказы", $this->model->getOrder());
	}

	// Изменение пароля от административной панели
	public function changeAction()
	{
		if (isset($_POST['password']) && $_POST['password']!=""){
			$this->model->setPass($_SESSION['admin'],md5($_POST['password']));
			$this->view->redirect("list");
		}
		$this->view->layout = "Admin";
		$this->view->render("Изменить пароль");
	}

	// Изменение пароля от административной панели
	public function adminsAction()
	{
		if (isset($_GET['delad'])){
			$this->view->layout = "Admin";
			$this->model->delAdmin($_GET['delad']);
			$this->view->render("Администратры", $this->model->getAdmins($_SESSION['admin']));
		}
		else{
			$this->view->layout = "Admin";
			$this->view->render("Администратры", $this->model->getAdmins($_SESSION['admin']));
		}
	}

	// Добавление администратора
	public function addadminAction()
	{
		if (isset($_POST['login']) && isset($_POST['password']) && ($_POST['login']!="") && ($_POST['password']!="")){
			$this->model->setAdmin($_POST['login'], md5($_POST['password']));
			$this->view->redirect("list");
		}
		else{
			$this->view->layout = "Admin";
			$this->view->render("Добавить администратора");
		}
	}
}
