<?php

namespace application\controllers;

use application\core\Controller;

class AccountController extends Controller {

	public function loginAction()
	{
		$_SESSION = array(); // или $_SESSION = array() для очистки всех данных сессии
		session_destroy();
		session_start();
		$this->view->layout = "Cabinet";
		$title = 'Вход в кабинет';
		if (isset($_POST['login']) && isset($_POST['password'])) {
			if (!$this->model->checkAdmin($_POST['login'], $_POST['password'])) {
				$title = 'Логин или пароль не верен!';
			} else {
				$_SESSION['admin'] = md5($_POST['login']);
				$this->view->redirect("./admin/list");
			}
		}
		$this->view->render($title);
	}

}
