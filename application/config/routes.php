<?php

return [

	// Главная страница
	'(/\?page=\d*)?' => [
		'controller' => 'main',
		'action' => 'index',
	],

	// Страница товара
	'/\?id=\d*' => [
		'controller' => 'main',
		'action' => 'product',
	],

	// Страница корзины
	'cart' => [
		'controller' => 'main',
		'action' => 'cart',
	],

	// Страница входа в админ панель
	'cabinet/login' => [
		'controller' => 'account',
		'action' => 'login',
	],


	// Админ панель со всеми товарами и возможностью удаления
	'cabinet/admin/list(\?delprod=\d*)?' => [
		'controller' => 'admin',
		'action' => 'list',
	],

	// Админ панель с добавлением товара
	'cabinet/admin/addproduct' => [
		'controller' => 'admin',
		'action' => 'add',
	],

	// Админ панель с изменением товара
	'cabinet/admin/product\?id=\d*' => [
		'controller' => 'admin',
		'action' => 'product',
	],

	// Админ панель с заказами
	'cabinet/admin/order?' => [
		'controller' => 'admin',
		'action' => 'order',
	],

	// Админ панель с изменением пароля
	'cabinet/admin/change' => [
		'controller' => 'admin',
		'action' => 'change',
	],

	// Админ панель со списком админов и возможностью удалить администратора
	'cabinet/admin/admins(\?delad=\d*)?' => [
		'controller' => 'admin',
		'action' => 'admins',
	],

	// Добавление администратора
	'cabinet/admin/addadmin' => [
		'controller' => 'admin',
		'action' => 'addadmin',
	],

];
