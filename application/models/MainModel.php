<?php

namespace application\models;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use application\core\Model;

class MainModel extends Model {

	/*Пролучение списка товаров*/
	public function getProducts($id) {
		$this->checkID($page);
		$id *= 3;

		$sql = "SELECT prod.`id_product`,im.`src` AS img FROM `products` prod JOIN `images` AS im ON prod.`id_product`=im.`id_product` WHERE prod.active = 1 AND prod.id_product > :id";
		$img = $this->db->row($sql, ['id' => $id]);

		$sql = "SELECT COUNT(id_product) count FROM products WHERE active = 1";
		$count = +$this->db->row($sql)[0]['count']/3;
		$count = floor($count);

		$sql = "SELECT * FROM products WHERE active = 1 AND id_product > :id LIMIT 3";
		$prod = [
			'products' => $this->db->row($sql, ['id' => $id]),
			'img' => $img,
			'count' => $count
		];
		return $prod;
	}

	// Установление и проверка id на integer
	public function checkID(&$id){
		settype($id, 'integer');
	}

	// Пролучение выбранного товара
	public function getMyProduct($id) {
		$this->checkID($id);

		// Получние фотографий
		$sql = "SELECT prod.`id_product`,im.`src` AS img FROM `products` prod JOIN `images` AS im ON prod.`id_product`=im.`id_product` WHERE prod.`id_product`=:id";
		$img = $this->db->row($sql,["id"=>$id]);

		// Получние товара
		$sql = "SELECT * FROM products WHERE `id_product`=:id";
		$prod = [
			'product' => $this->db->row($sql,["id"=>$id])[0],
			'img' => $img,
		];
		return $prod;
	}

	//  Получение похожих товаров
	public function similarProduct($id) {
		$this->checkID($id);

		// Похожие товары
		$sql = "SELECT prod.`id_product`, prod.`name_product`,prod.`price`,prod.`active` FROM `products` prod JOIN `similar_t` sim ON prod.`id_product`=sim.`id_similar_product` WHERE sim.`id_product`=:id";
		$sim = $this->db->row($sql,["id"=>$id]);

		$img="";
		if (isset($sim[0])){
			// Картинки похожих товаров
			$sql = "SELECT prod.`id_product`,img.`src` AS img FROM `products` prod JOIN `similar_t` sim ON prod.`id_product`=sim.`id_similar_product` JOIN images AS img ON prod.`id_product`=img.id_product WHERE sim.`id_product`=:id";
			$img = $this->db->row($sql,["id"=>$id]);
		}

		$sim = [
			'similar' => $sim,
			'img' => $img
		];

		return $sim;
	}

	// Поучение товаров из корзины
	public function getCartProduct($prodId){
		$sql = "SELECT id_product AS id, name_product AS name, price FROM products WHERE id_product = :id";
		$sqlImg = "SELECT src FROM `images` WHERE id_product = :id LIMIT 1";
		$count = array_count_values($prodId);
		$prodId = array_unique($prodId);
		$arr = [];
		foreach ($prodId as $id){
			$prod = $this->db->row($sql,["id"=>$id]);
			$img = $this->db->row($sqlImg,["id"=>$id]);
			array_push($arr, ['id' => $prod[0]['id'], 'name' => $prod[0]['name'], 'price' => $prod[0]['price'], 'count' => $count[$id], 'img' => $img[0]['src']]);
		}
		if (!empty($arr)) {
			$arr = [
				'products' => $arr
			];
		}
		return $arr;
	}

	public function addOrder($id, $price, $count, $name, $phone, $email){
		$sql = "SELECT MAX(`id_order`) AS max FROM `orders`";
		$max = (int)$this->db->row($sql)[0]['max'] + 1;

		$sql = "INSERT INTO `orders`(`id_order`, `product_id`, `price`, `count`, `name`, `phone`, `email`) VALUES ($max, $id, $price, $count, :name, :phone, :email)";
		$this->db->row($sql, ['name' => $name, 'phone' => $phone, 'email' => $email]);
		return $max;
	}

	public function send_post($email, $name, $prodAll, $priceAll, $id) //отправление на почту заказчика
	{
		$mail = new PHPMailer(true);                              // Passing true enables exceptions
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'smtp.yandex.ru';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'macsim-1996@yandex.ru';                 // SMTP username
		$mail->Password = '!@#$%^&*()_+';                           // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, ssl also accepted
		$mail->Port = 587;                                    // TCP port to connect to

		//Recipients
		$mail->setFrom('macsim-1996@yandex.ru', 'Mailer');
		$mail->addAddress($email, $name);     // Add a recipient
		//Content
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = mb_convert_encoding ('Уведомление о заказе',  "UTF-8");
		$mail->Body    = mb_convert_encoding ('<p><strong>Заказ получен от магазина GREATFATION, мы скоро свяжемся с вами '.$name.'! </strong></p><p>Ваш заказ: '.$prodAll.' на сумму:  '.$priceAll.' руб.</p><p><strong>Ваш номер заказа: '.$id.' </strong></p>',
			"UTF-8");


		$mail->send();

		return $mail;
	}
}
