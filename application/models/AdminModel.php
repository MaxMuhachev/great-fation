<?php

namespace application\models;

use application\core\Model;

class AdminModel extends Model {

	// Установление и проверка id на integer
	public function checkID(&$id){
		settype($id, 'integer');
	}

    // Пролучение списка товаров
    public function getProducts() {
        $sql = "SELECT prod.`id_product`,im.`src` AS img FROM `products` prod JOIN `images` AS im ON prod.`id_product`=im.`id_product`";
        $img = $this->db->row($sql);
        $sql = "SELECT * FROM products";
        $prod = [
            'products' => $this->db->row($sql),
            'img' => $img
        ];

        return $prod;
    }

	// Пролучение списка товаров
	public function getAllProducts() {
		$sql = "SELECT id_product, name_product FROM products";
		$prod = [
			'products' => $this->db->row($sql),
		];

		return $prod;
	}

	// Пролучение списка возможных похожих товаров
	public function getSimilarProducts($id) {
		$sql = "SELECT * FROM products WHERE id_product NOT IN (SELECT id_similar_product AS sim FROM similar_t WHERE id_product=:id GROUP BY similar_t.id_similar_product) AND NOT(id_product=:id)";
		$prod = $this->db->row($sql, ["id" => $id]);

		$sql = "SELECT `src`, `id_product` FROM `images` WHERE `id_product`=:id";
		$img = "";
		if (isset($prod[0])){
			$img = $this->db->row($sql,['id' => $prod[0]['id_product']]);
		}

		$res = [
			'products' => $prod,
			'img' => $img
		];

		return $res;
	}

	// Добваление похожего товара
	public function addSimilarProducts($name, $prod_id) {
		$sql = "UPDATE `products` SET `similar` = 1 WHERE `id_product`=:prod_id";
		$prod = $this->db->query($sql, ["prod_id" => $prod_id]);

		$sql = "SELECT * FROM `products` WHERE `name_product`=:name";
		$prod = $this->db->row($sql, ["name" => $name]);

		$sql = "INSERT INTO `similar_t`(`id_product`, `id_similar_product`) VALUES (:prod_id, :id_similar)";
		$this->db->query($sql, ["prod_id" => $prod_id, 'id_similar' => $prod[0]['id_product']]);

		$sql = "SELECT `src`, `id_product` FROM `images` WHERE `id_product`=:id";
		$img = $this->db->row($sql,['id' => $prod[0]['id_product']]);

		$res = [
			'products' => $prod,
			'img' => $img
		];

		return $res;
	}

	//  Удаление похожих товаров
	public function delSimProducts($sim_id, $prod_id) {
		$this->checkID($sim_id);
		$this->checkID($prod_id);

		$sql = "DELETE FROM `similar_t` WHERE `id_similar_product`=:sim_id AND `id_product`=:prod_id";
		$this->db->query($sql,["sim_id" => $sim_id,'prod_id' => $prod_id]);

		$sql = "SELECT * FROM `similar_t` WHERE `id_product`=:prod_id";
		$prod = $this->db->row($sql, ["prod_id" => $prod_id]);

		$sim = (isset($prod[0])) ? 1 : 0;
		$sql = "UPDATE `products` SET `similar` = {$sim} WHERE `id_product`=:prod_id";
		$this->db->query($sql, ["prod_id" => $prod_id]);
	}

	// Пролучение выбранного товара
	public function getMyProduct($id) {
		$this->checkID($id);

		// Получние фотографий
		$sql = "SELECT prod.`id_product`,im.`src` AS img FROM `products` prod JOIN `images` AS im ON prod.`id_product`=im.`id_product` WHERE prod.`id_product`=:id";
		$img = $this->db->row($sql,["id"=>$id]);

		// Получние товара
		$sql = "SELECT * FROM products WHERE `id_product`=:id";
		$prod = [
			'product' => $this->db->row($sql,["id"=>$id])[0],
			'img' => $img,
		];
		return $prod;
	}

	//  Получение похожих товаров
	public function similarProduct($id) {
		$this->checkID($id);

		// Похожие товары
		$sql = "SELECT prod.`id_product`, prod.`name_product`,prod.`price`,prod.`active` FROM `products` prod JOIN `similar_t` sim ON prod.`id_product`=sim.`id_similar_product` WHERE sim.`id_product`=:id";
		$sim = $this->db->row($sql,["id"=>$id]);

		$img="";
		if (isset($sim[0])){
			// Картинки похожих товаров
			$sql = "SELECT prod.`id_product`,img.`src` AS img FROM `products` prod JOIN `similar_t` sim ON prod.`id_product`=sim.`id_similar_product` JOIN images AS img ON prod.`id_product`=img.id_product WHERE sim.`id_product`=:id";
			$img = $this->db->row($sql,["id"=>$id]);
		}

		$sim = [
			'similar' => $sim,
			'img' => $img
		];

		return $sim;
	}

	//Добавление товара
	public function addProduct($name, $description, $photos, $price, $active){
		$active = ($active == 'Активно') ? 1 : 0;
		$sql = "INSERT INTO `products`(`name_product`, `description`, `price`, `active`)
VALUES (:name, :description, :price, :active)";
		$this->db->query($sql,["name" => $name, 'description' => $description, 'price' => $price, 'active' => $active]);

		$sql = "SELECT MAX(id_product) AS max FROM products";
		$prod = $this->db->row($sql)[0]['max'];

		$sql = "UPDATE `similar_t` SET `id_product` = :id WHERE `id_product` = 0";
		$this->db->query($sql,['id' => $prod]);

		$uploadDir = './public/images/';
		for ($i = 0; $i< count($photos['name']); $i++){
			$uploadFile = $uploadDir . basename($photos['name'][$i]);
			if (move_uploaded_file($photos['tmp_name'][$i], $uploadFile)) {
				$sql = "INSERT INTO `images`(`src`, `id_product`) VALUES (:src, $prod)";
				$this->db->query($sql,["src" => $photos['name'][$i]]);
			}
		}

	}

	//Изменение товара
	public function updateProduct($name, $description, $photos, $price, $active, $id){
		$active = ($active == 'Активно') ? 1 : 0;
		$sql = "UPDATE `products` SET `name_product` =:name, `description` = :description,
                      `price` = :price, `active` = :active WHERE `id_product` = :id";
		$this->db->query($sql,["name" => $name, 'description' => $description, 'price' => $price,
			'active' => $active, 'id' => $id]);

		$uploadDir = './public/images/';
		for ($i = 0; $i< count($photos['name']); $i++){
			$uploadFile = $uploadDir . basename($photos['name'][$i]);
			if (move_uploaded_file($photos['tmp_name'][$i], $uploadFile)) {
				$sql = "INSERT INTO `images`(`src`, `id_product`) VALUES (:src, $id)";
				$this->db->query($sql,["src" => $photos['name'][$i]]);
			}
		}
	}

	//Удаление картики товара
	public function delImage($id, $src){
		unlink('./public/images/'.$src);
		$sql = "DELETE FROM `images` WHERE `id_product`=:id AND `src`=:src";
		$this->db->query($sql,["id" => $id, 'src' => $src]);
	}

	// Удаление товара
	public function delProduct($id) {
		$sql = "DELETE FROM `products` WHERE `id_product`=:id";
		$this->db->query($sql,["id" => $id]);
	}

	// Получение всех заказов
	public function getOrder() {
		$sql = "SELECT ord.id_order, ord.price, ord.count, ord.name, ord.phone, ord.email, pr.id_product, pr.name_product 
					FROM orders ord JOIN products pr ON ord.product_id=pr.id_product  ORDER BY  ord.id_order";
		$order = [
			'order' => $this->db->row($sql)
		];
		return $order;
	}

	// Получение всех администраторов
	public function getAdmins($login) {
		$sql = "SELECT `id_users`,`login` FROM users JOIN role ON users.role_id=role.id_role WHERE role.name_role='admin' AND NOT(md5(users.login)=:login) ";
		$order = [
			'admin' => $this->db->row($sql,['login' => $login])
		];
		return $order;
	}

	// Добавление администратора
	public function setAdmin($login, $password) {
		$sql = "INSERT INTO `users`( `login`, `password`, `role_id`) VALUES (:login, :password, 1)";
		$this->db->query($sql,['login' => $login,'password' => $password]);
	}

	// Изменение пароля администратора, который вошёл
	public function setPass($login, $password) {
		$sql = "UPDATE `users` SET `password` = :password WHERE md5(`login`) = :login";
		$this->db->query($sql,['password' => $password, 'login'=> $login]);
	}

	// Удаление администратора
	public function delAdmin($id) {
		$this->checkID($id);

		$sql = "DELETE FROM users JOIN role ON users.role_id=role.id_role WHERE role.name_role='admin' WHERE users.`id_users`=:id";
		$this->db->query($sql,["id" => $id]);
	}
}
