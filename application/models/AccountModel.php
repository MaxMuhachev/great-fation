<?php


namespace application\models;

use application\core\Model;

class AccountModel extends Model {

    /*Провнрка существования пользователя*/
    public function checkAdmin($login, $password) {
        $sql = "SELECT `id_users` FROM users JOIN role ON users.role_id=role.id_role WHERE login = :login AND password = :password AND role.name_role='admin'";
        $query = $this->db->row($sql,["login" => $login, "password" => md5($password)]);
        return (!empty($query));
    }
}
