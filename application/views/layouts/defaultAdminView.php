<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title; ?></title>
    <meta http-equiv="Content-Type" Content="text/html; Charset=UTF8">
    <link type="text/css" rel="stylesheet" href="../../public/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="../../public/fonts/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="../../public/css/styleCabinet.css">

</head>
<body>

<div class="container-fluid ">
    <div class="row">
        <div class="col-sm  menu">
	        <span class="market_name">great fashion</span>
	        <nav>
		        <input type="checkbox" name="menu" id="btn-menu" class="menuBut"/>
		        <label for="btn-menu">Меню</label>
		        <ul>
			        <li><a class="dropdown-item
		              <?php
						        echo ($this->route['action']=='add' || $this->route['action']=='list'|| $this->route['action']=='product') ? "active" : "";
							          ?>
									" href="list">Товары</a></li>
			        <li><a class="dropdown-item <?php echo ($this->route['action']=='order') ? "active" : "";?>" href="order">Список заказов</a></li>
			        <li><a class="dropdown-item <?php echo ($this->route['action']=='change') ? "active" : "";?>" href="change">Смена пароля от админ панели</a></li>
			        <li><a class="dropdown-item <?php echo ($this->route['action']=='admins') ? "active" : "";?>"href="admins">Список администраторов</a></li>
			        <li><a class="dropdown-item "href="../login">Выйти</a></li>
		        </ul>
	        </nav>

        </div>

        <div class="col-sm col-xs-50 col-sm-12 col-md-12 col-lg-9">
            <div class="container">
	            <h1><?php echo $title; ?></h1>
	            <?php echo $content; ?>
            </div>
        </div>
    </div>
</div>
<script src="../../public/js/jquery.js"></script>
<script src="../../public/js/bootstrap.min.js"></script>
<script src="../../public/js/scriptCabinet.js"></script>
</body>
</html>
