<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
	<meta http-equiv="Content-Type" Content="text/html; Charset=UTF8">
	<link type="text/css" rel="stylesheet" href="./public/css/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" href="./public/fonts/font-awesome.min.css">
	<link type="text/css" rel="stylesheet" href="./public/css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<style>
		.under{
			padding-left: 50px;
		}
	</style>
</head>
<body>

<!--Header-->
<header>
	<nav class="navbar navbar-expand-lg navbar-light bg-light text-uppercase">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="#">О компании</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Оплата и доставка</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Помощь</a>
				</li>
			</ul>
		</div>
		<a class="navbar-brand" href="./">greatfation</a>
	</nav>

	<div class="container-fluid p-0">
		<a href="cart">
			<i class="fa fa-shopping-cart fa-2x" aria-hidden="true">
				<span class="cart text-uppercase"> Корзина (<?php echo $cart; ?>)</span>
			</i>
		</a>
		<hr>
	</div>
</header>
	<!--Header-->

		<?php echo $content; ?>


	</div>

	<!-- Footer -->
	<footer class="page-footer font-small pt-4">
		<div class="container">
			<div class="row">
				<div class="col-xs-50 col-sm-12 col-md-12 col-lg-12">
			<?php
				if (isset($similar) && (!empty($similar['img'][0]))){
					echo "<h4 class='text-uppercase like'>Вам так же понравится</h4>";
					foreach ($similar['similar'] as $sim) {
						echo "<div class=\"block\"><div class='add'><button type=\"button\" class=\"btn btn-light text-uppercase\"  onclick='addCart(this)'>В корзину</button></div>";
						echo "<a href='?id={$sim['id_product']}'>";
						foreach ($similar['img'] as $image) {
							if (isset($image['id_product']) && ($sim['id_product'] == $image['id_product'])){
								echo "<img src='./public/images/{$image["img"]}'>";
								break;
							}
						}
						echo "";
						echo "<div>".$sim['name_product']."</div></a>";
						echo "<p class='price'>".$sim['price']." <i class=\"fa fa-ruble\" aria-hidden=\"true\"></i></p></div>";
					}
				}

			?>
				</div>
			</div>
		</div>
		<div class=" under">
			GREATFATION - лидер на отечественно рынке. Основу ассортимента составляют:
			женская одежда, мужская одежда, детская одежда, обувь, аксессуары. Мы предлагаем вам огромный выбор недорогой и качественной одежды популяных производителей. У нас вы можете преобрести обновки на любой случай; от торжественных церемоний до занятий спортом.
		</div>

		<hr>

		<!-- Footer Links -->
		<div class="container text-center text-md-left">

			<!-- Grid row -->
			<div class="row">

				<!-- Grid column -->
				<div class="container col-md-8 mt-md-0 mt-3">

					<!-- Content -->
					<h5 class="text-uppercase">Присоединяйся к нам</h5>
					<p>
						<a href="#"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-vk fa-2x" aria-hidden="true"></i></a>
					</p>

				</div>
				<!-- Grid column -->

				<!-- Grid column -->
				<div class="col-md-3 mb-md-0 mb-3">

					<!-- Links -->
					<h5 class="text-uppercase pay">Принемаем к оплате</h5>

					<p>
						<a href="#"><i class="fa fa-cc-visa fa-2x" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-cc-mastercard fa-2x" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-cc-paypal fa-2x" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-credit-card-alt fa-2x" aria-hidden="true"></i></a>
					</p>


				</div>
				<!-- Grid column -->



			</div>
			<!-- Grid row -->

		</div>
		<!-- Footer Links -->

		<!-- Copyright -->
		<div class="footer-copyright text-center py-3 text-uppercase">© 2019 Greatfation</div>
		<!-- Copyright -->

	</footer>
	<!-- Footer -->

	<script src="./public/js/jquery.js"></script>
	<script src="./public/js/script.js"></script>
	<script src="./public/js/bootstrap.min.js"></script>

</body>
</html>
