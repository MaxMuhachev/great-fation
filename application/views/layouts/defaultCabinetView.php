<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
	<meta http-equiv="Content-Type" Content="text/html; Charset=UTF8">
	<link type="text/css" rel="stylesheet" href="../public/css/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" href="../public/fonts/font-awesome.min.css">
	<link type="text/css" rel="stylesheet" href="../public/css/styleCabinet.css">

</head>
<body>
	<?php echo $content; ?>
<script src="../public/js/jquery.js"></script>
<script src="../public/js/bootstrap.min.js"></script>
</body>
</html>
