<?php
	/*
	* Список товаров
	*/
?>

		<div class="button">
			<a href="addproduct"><input type="button" name="add_product" class="btn btn-success" value="Добавить товар" onclick=""></a>
		</div>

		<table class="table table-striped table-bordered table-hover tableID">
        <thead>
        <tr>
          <th>Изменить</th>
          <th>№</th>
          <th>Название товара</th>
          <th>Описание</th>
          <th>Картинки</th>
          <th>Цена</th>
          <th>Статус</th>
          <th>Похожие</th>
          <th>Удалить</th>
        </tr>
        </thead>
        <tbody>

          <?php
          foreach ($products as $product) {
              echo "<tr>
											<td>
												<button type=\"button\" class=\"btn btn-outline-warning\"><a href='./product?id=".$product['id_product']."'>Изменить</a></button>
											</td>
											<th>".$product['id_product']."</th>";
              echo "<th>".$product['name_product']."</th>";
              echo "<td>".$product['description']."</td>";
              echo "<td>";
              foreach ($img as $image) {
                  if (isset($image['id_product']) && ($product['id_product'] == $image['id_product'])){
                      echo "<img src='../../public/images/{$image["img"]}'class='imgAdmin'>";
                  }
              }
              echo "</td>";
              echo "<td>".$product['price']."</td>";
              echo ($product['active']) ? "<td>А" : "<td>Не а";
              echo "ктивно</td><td>";
              echo $product['similar']!="0" ? "Да" : "Нет";
              echo "</td><td><a href='list?delprod={$product['id_product']}'><button type=\"button\" class=\"btn btn-outline-danger\">Удалить</button></a></td></tr>";
          }
          ?>

        </tbody>
    </table>
