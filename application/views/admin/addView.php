<?php
	/*
	* Добавление товара
	*/
?>

<form action="" method="post" enctype="multipart/form-data">

	<input type="hidden" id="product_id"value="0">

	<div class="form-group">
		<label for="exampleFormControlInput1">Название</label>
		<input type="text" class="form-control" id="name" name="name" placeholder="Название товара">
	</div>

	<div class="form-group">
		<label for="exampleFormControlTextarea1">Описание</label>
		<textarea class="form-control" name="description" rows="15"></textarea>
	</div>

	<div class="form-group">
		<p>Фотографии</p>
		<div class="mb-3">
				<input type="file" multiple="multiple" name="photo[]">
		</div>
	</div>

	<div class="form-group">
		<label for="exampleFormControlInput1">Цена</label>
		<input type="number" class="form-control" name="price" placeholder="Цена товара"">
	</div>

	<div class="form-group">
		<label for="exampleFormControlSelect1">Состояние</label>
		<select class="form-control" id="active" name="active">
			<option>Активно</option>
			<option>Не активно</option>
		</select>
	</div>


	<div class="form-group">
		Похожие
	</div>
	<table class="table table-striped table-bordered table-hover tableID">
		<thead>
		<tr>
			<th>Убрать из похожих</th>
			<th>№</th>
			<th>Название товара</th>
			<th>Картинки</th>
			<th>Цена</th>
			<th>Активно</th>
		</tr>
		</thead>
		<tbody>

		</div>
		</tbody>
	</table>

	<div class="form-group">
		<label for="exampleFormControlSelect1">Добавить в похожие</label>
		<select class="form-control" id="similar">
		<?php
			foreach ($products as $all_products){
				echo "<option>{$all_products['name_product']}</option>";
			}
		?>
		</select>
	</div>
	<input type="button" name="add_sim" class="btn btn-secondary" value="Добавить" onclick="addSim()">
	<input type="submit" class="btn btn-success change" value="Добавить">

</form>


