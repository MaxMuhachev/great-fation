<?php
	/**
	 * Изменение пароля от админ панели
	 */
?>
<form action="" method="post" class="form-inline" style="margin-top: 12%">
	<div class="form-group mb-2">
		<label for="staticEmail2" class="sr-only">Пароль</label>
		<input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="Пароль:">
	</div>

	<div class="form-group mx-sm-3 mb-2">
		<label for="inputPassword2" class="sr-only">Пароль</label>
		<input type="password" class="form-control" id="password" name="password" placeholder="Новый пароль">
		<button type="submit" class="btn btn-success" style="margin-left:20px">Изменить</button>
	</div>
</form>


