<?php
	/**
	 * Список администраторов
	 */
?>
<p><a href="addadmin"><input type="button" name="add_product" class="btn btn-success" value="Добавить администратора" onclick=""></a></p>
<table class="table table-striped table-bordered table-hover tableID">
	<thead>
	<tr>
		<th>Логин администратора</th>
		<th>Удалить</th>
	</tr>
	</thead>
	<tbody>

	<?php
		foreach ($admin as $admin) {
			echo "<tr><td>{$admin['login']}</td>";
			echo "<td><b><a href='?delad={$admin['id_users']}'><button type=\"button\" class=\"btn btn-outline-danger\">Удалить</button></a></b></td></tr>";
		}
	?>

	</tbody>
</table>
