<?php
	/*
	* Изменение товара
	*/
?>

<form action="" method="post" enctype="multipart/form-data">
	<input type="hidden" id="product_id"value="<?php echo $product['product']['id_product'];?>" name="product_id">
			<div class="form-group">
				<label for="exampleFormControlInput1">Название</label>
				<input type="text" class="form-control" name="name" placeholder="Название товара" value="<?php echo $product['product']['name_product'];?>">
			</div>

			<div class="form-group">
				<label for="exampleFormControlTextarea1">Описание</label>
				<textarea class="form-control" name="description" rows="15"><?php echo $product['product']['description']?></textarea>
			</div>
			<div class="form-group">
				<p>Фотографии</p>
				<div>

				<?php
					foreach ($product['img'] as $image) {
						if (isset($image['id_product']) && ($product['product']['id_product'] == $image['id_product'])){
							echo "<span><img src='../../public/images/{$image["img"]}' class='imgAdmin'>";
							echo "<img src='../../public/images/del.png' class='delButton' onclick='deleteImage(this)'></span>";
						}
					}
				?>
				</div>
			</div>

			<div class="form-group">
				<p>Добавить фотографии</p>
				<div class="mb-3">
					<input type="file" multiple="multiple" name="photo[]">
				</div>
			</div>

			<div class="form-group">
				<label for="exampleFormControlInput1">Цена</label>
				<input type="number" class="form-control" name="price" placeholder="Цена товара" value="<?php echo $product['product']['price'];?>">
			</div>

			<div class="form-group">
				<label for="exampleFormControlSelect1">Состояние</label>
				<select class="form-control" name="active">
					<option>Активно</option>
					<option>Не активно</option>
				</select>
			</div>

			<div class="form-group">
				Похожие
			</div>
			<table class="table table-striped table-bordered table-hover tableID">
				<thead>
				<tr>
					<th>Убрать из похожих</th>
					<th>№</th>
					<th>Название товара</th>
					<th>Картинки</th>
					<th>Цена</th>
					<th>Активно</th>
				</tr>
				</thead>
				<tbody>

					<?php

						foreach ($similar['similar'] as $sim) {
							echo "<tr id=\"{$sim['id_product']}\">";
							?>
							<td>
									<button type="button" class="btn btn-outline-danger" name="<?php echo $sim['id_product'];?>" onclick="delteSimilar(this)">Удалить</button>
							</td>
							<?php
								echo "<td>{$sim['id_product']}</td>";
								echo "<td>".$sim['name_product']."</td>";
								echo "<td>";
								foreach ($similar['img'] as $imgSim) {
									if ($sim['id_product'] == $imgSim['id_product']){
								    echo "<img src='../../public/images/{$imgSim["img"]}' class='imgAdmin'>";
							    }
								}
								echo "</td>";
								echo "<td>".$sim['price']."</td>";
								if ($sim['active']){
									echo "<td>Да</td>";
								}
								else{
									echo "<td>Нет</td>";
								}
								echo "</tr>";
						}
						?>
				</div>
				</tbody>
			</table>

			<div class="form-group">
				<label for="exampleFormControlSelect1">Добавить в похожие</label>
				<select class="form-control" id="similar">
					<?php
						foreach ($all['products'] as $all_products){
							echo "<option>{$all_products['name_product']}</option>";
						}
					?>
				</select>
			</div>
	<input type="button" name="add_sim" class="btn btn-secondary" value="Добавить" onclick="addSim()">


	<input type="submit" class="btn btn-success change" value="Изменить">

</form>
