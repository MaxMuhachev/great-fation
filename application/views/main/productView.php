<div class="container">
	<?php
		echo "<h1>$title</h1><br><br>";
		$prod = $product['product'];
		echo "<h3>{$prod['name_product']}</h3>";
		$price = number_format($prod['price'],  2, '.',' ');
		$price = substr($price, 0, strlen($price) - 3);
		$i = 0;
	?>
	<!--Карусель изображений-->
	<div class="container-fluid prodImg">
		<div class="carousel slide" id="car" data-ride="carousel">
			<div class="carousel-inner">
					<?php
						foreach ($product['img'] as $img){
							if ($i != 0){
								echo "<div class=\"carousel-item\">";
							}
							else{
								echo "<div class=\"carousel-item active \">";
							}
							echo "<img src=\"./public/images/{$img['img']}\" class=\"d-block w-100\">";
							echo "</div>";
							$i++;
						}
					?>
			</div>
			<a href="#car" class="carousel-control-prev" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a href="#car" class="carousel-control-next" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
	<!--Карусель изображений-->
</div>

<?php
	echo "<div class='priceDiv'>Цена: <span class='price'>$price <i class=\"fa fa-ruble\" aria-hidden=\"true\"></i></span></div>";
  echo "<div class='add' style='display: contents;'><button type=\"button\" class=\"btn btn-light text-uppercase\"  data-toggle=\"modal\" data-target=\"#exampleModalCenter\" onclick='addCart(this)' id='{$prod['id_product']}'>В корзину</button></div>";
	echo "<h4>Описание</h4><div class='description container'>{$prod['description']}</div>";
	?>


