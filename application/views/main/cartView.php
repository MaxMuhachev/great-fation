<div class="container content">
	<form action="" method="post" class="form-row">
		<h1 class="m-t-50"><?php echo $title; ?></h1>
		<table class="table  tableID">
			<thead>
			<tr>
				<th>Фото</th>
				<th>Название</th>
				<th>Количество</th>
				<th>Сумма</th>
				<th>Действие</th>
			</tr>
			</thead>
			<tbody>
				<?php
					$price = 0;
					$priceNum = 0;
					$i = 0;
					if (isset($products))
						foreach ($products as $product) {
							$price = number_format($product['price'],  2, '.',' ');
							$price = substr($price, 0, strlen($price) - 3);
							echo "<tr  class='{$product['id']}'><td><a href='./?id={$product['id']}'><image src='./public/images/{$product['img']}' class='img'></a></td>";
							echo "<td>{$product['name']}</td>";
							echo "<td><input type=\"number\" class=\"count\" name=\"$i\" placeholder=\"Количество\" value=\"{$product['count']}\" onchange='setPrice(this)'></td>";
							echo "<td class='price'>{$price} <i class=\"fa fa-ruble\" aria-hidden=\"true\"></i></td>";
							echo "<td><button type=\"button\" id='{$product['id']}' class=\"btn btn-danger\" onclick='delCart(this)'>Удалить</button></td></tr>";
							$priceNum += $product['price'];
							$i++;
						}
					$price = number_format($priceNum,  2, '.',' ');
					$price = substr($price, 0, strlen($price) - 3);
				?>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>Итого: <span class="price itog"><span class="res"><?php echo $price;?></span> <i class="fa fa-ruble" aria-hidden="true"></i></span></td>
				</tr>
			</tbody>
		</table>

		<div class="form-group mb-0">
			<input type="text" readonly class="form-control-plaintext w-25" value="Имя" >
			<input type="text" class="form-control w-75" id="name" name="name" placeholder="Имя">
		</div>

		<div class="form-group mb-0">
			<input type="text"   readonly class="form-control-plaintext w-25" value="Почта:" >
			<input type="email" class="form-control w-75" id="email" name="email" placeholder="eample@mail.ru">
		</div>
		<div class="form-group mb-0">
			<input type="text"   readonly class="form-control-plaintext w-50" value="Телефон:" >
			<input type="number" class="form-control w-75" id="phone" name="phone" placeholder="89655738071">
		</div>

		<div class="submit">
			<input type="submit" class="btn btn-light text-uppercase zak" value="оформить заказ">
		</div>


	</form>

