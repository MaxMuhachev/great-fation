<!--Карусель изображений-->
<div class="container p-0">
	<div class="carousel slide" id="car" data-ride="carousel">
		<ol class="carousel-indicators">
			<li class="active" data-target="#car" data-slide-to="0"></li>
			<li data-target="#car" data-slide-to="1"></li>
			<li data-target="#car" data-slide-to="2"></li>
		</ol>
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img src="./public/images/8.jpg" alt="" class="d-block w-100">
			</div>
			<div class="carousel-item">
				<img src="./public/images/9.jpg" alt="" class="d-block w-100">
			</div>
			<div class="carousel-item">
				<img src="./public/images/6.jpg" alt="" class="d-block w-100">
			</div>
		</div>
		<a href="#car" class="carousel-control-prev" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a href="#car" class="carousel-control-next" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</div>
<!--Карусель изображений-->

<div class="container content">


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Товар добавлен!</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				Товар добавлен в корзину!
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
				<a href="cart"><button type="button" class="btn btn-primary">В корзину</button></a>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->

<!-- Content -->
	<div class="row">
		<div class="col-xs-50 col-sm-12 col-md-12 col-lg-12">
			<?php
			foreach ($products as $product) {
			  $price = number_format($product['price'],  2, '.',' ');
			  $price = substr($price, 0, strlen($price) - 3);
			  echo "<div class=\"block\"><div class='add'>";
			  echo "<button type=\"button\" class=\"btn btn-light text-uppercase\" data-toggle=\"modal\" data-target=\"#exampleModalCenter\" onclick='addCart(this)' id='{$product['id_product']}'>В корзину</button></div>";
			  echo "<a href='?id=".$product['id_product']."'>";
					foreach ($img as $image) {
						if (isset($image['id_product']) && ($product['id_product'] == $image['id_product'])){
							echo "<img src='./public/images/{$image["img"]}'>";
							break;
						}
					}
				echo "";
			  echo "<div>".$product['name_product']."</div></a>";
			  echo "<p class='price'>".$price." <i class=\"fa fa-ruble\" aria-hidden=\"true\"></i></p></div>";
			}
			?>
		</div>
	</div>
	<div class="page">
		<span class="p-3">Страницы: </span>
			<?php
				for ($i = 1; $i <= $count+1; $i++){
					echo "<a href='?page=".($i-1)."'><button type=\"button\" class=\"btn btn-light\">$i</button></a>";
				}
			?>
	</div>
<!-- Content -->

<?php
