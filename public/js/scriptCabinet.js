//  Удалениепохожих товаров
function delteSimilar(button) {
  $.ajax({
    url: '',
    type: 'POST',
    data: {'delSimilar':button.name,  "prod_id": $('input#product_id')[0].value},
    success: function(){
      var td = 'tr[id="'+ button.name +'"]>td';
      var option = "<option>" + $(td)[2].textContent + "</option>";
      $('select#similar:last-child').append(option);

      var tr = 'tr[id="'+ button.name +'"]';
      $(tr).remove();

    }
  });
}

// Добавление похожих товаров
function addSim() {
  if (typeof $('select#similar')[0].value !== 'undefined'){
    $.ajax({
      url: '',
      type: 'POST',
      data: {'addSimilar': $('select#similar')[0].value,  "prod_id": $('input#product_id')[0].value},
      success: function(data){

        data = JSON.parse(data);

        table = "<tr id='" + data['products'][0]['id_product'] + "'><td><button type=\"button\" class=\"btn btn-outline-danger\" name=\"" + data['products'][0]['id_product'] + "\" onclick=\"delteSimilar(this)\">Удалить</button></td>";
        table += "<td>" + data['products'][0]['id_product'] + "</td><td>" + data['products'][0]['name_product'] + "</td><td>";
        data['img'].forEach(function(image, i, data){
          table += "<img src='../../public/images/" + image['src'] + "'class='imgAdmin'>";
        });
        table += '</td><td>'+ data['products'][0]['price'] +'</td><td>';
        table += data['products']['active'] ? "Да" :"Нет";
        table += "</td></tr>"

        $('tbody:last-child').append(table);
        $("#similar :selected").remove();
      }
    });
  }
}

//При нажатии удалить картинку
function deleteImage(img) {
  var src = $(img).closest('span')[0].firstChild.src.split("/");
  src = src[src.length-1];

  $.ajax({
    url: '',
    type: 'POST',
    data: {'src': src,  "prod_id": $('input#product_id')[0].value},
    success: function(data){
      $(img).closest('span').remove();
    }
  });
}

// Для полной высоты меню
$(document).ready(function() {
  if ($(".menu").height()+1 < $(document).height() && $(document).width()>1320) {
    $(".menu").height('-webkit-fill-available');
  }

});
