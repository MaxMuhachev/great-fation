//Для вывода "В корзину" на карточках товарах
$('.block').hover(function() {
    $(this).children(".add").show();
  },
  function() {
    $(this).children(".add").hide();
  });

//Для вывода "В корзину" на карточках товарах
$('.block>img').hover(function() {
    $(this).parent().children(".add").show();
  },
  function() {
    $(this).parent().children(".add").hide();
  });


//Добавление товара в корзину
function addCart(but) {
  $.ajax({
    url: '',
    type: 'POST',
    data: {'cart_id': but.id},
    success: function(data){
      var cart = $('.cart')[0].innerHTML.split("(");
      var count = Number(cart[1].split(")")[0]) + 1;
      document.querySelector(".cart").innerHTML=cart[0] + "(" + count + ")";
    }
  });
}

//Удаление товара из корзины
function delCart(but) {
  $.ajax({
    url: '',
    type: 'POST',
    data: {'clean': but.id},
    success: function(data){
      var tr = "tr." + but.id;
      $(tr).remove();
      var cart = $('.cart')[0].innerHTML.split("(");
      var count = Number(cart[1].split(")")[0]) - 1;
      document.querySelector(".cart").innerHTML=cart[0] + "(" + count + ")";
      if(setPrice() == 0){
        $("input").remove();
      };
    }
  });
}

//Получение общей цены в корзине
function setPrice() {
  var arrPrice = $('.price');
  var count =  $('.count');
  var res = 0;
  var compound = [];
  var numPrice = [];

  for (var i = 0; i < arrPrice.length-1; i++){
    compound = arrPrice[i].innerText.split(" ")
    numPrice = compound[0] + compound[1];

    res += (+numPrice * count[i].value);
  }
  document.querySelector(".res").innerHTML = res + "";
  return res;
}

//Для пустой корзины
function cartClean() {
  if ($("tr").length <= 2){
    $("input").remove();
  }
}
